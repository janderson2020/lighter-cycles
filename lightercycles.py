import pyglet
from pyglet.gl import *
from pyglet.window import key
import random

#Dimensions of the playable screen
window_width  = 600
window_height = 600
cell_size = 10

# Bounding Box
brdr_sz = 5

columns = window_width / cell_size
rows = window_height / cell_size

global cells, current_loc_red, current_dir_red, current_loc_blue, current_dir_blue, alive_red, alive_blue

controls_img = pyglet.image.load('controls_lightercycles.png').texture
state = 'start'


win = pyglet.window.Window(window_width+2*brdr_sz, window_height+2*brdr_sz)

def draw_controls():
    global controls_img
    
    glColor3f(1,1,1)
    controls_img.blit(150,100)

def startup():
    global cells, current_loc_red, current_dir_red, current_loc_blue, current_dir_blue, alive_red, alive_blue, paused 
    cells = [[0] * columns for i in range(rows)]
    current_loc_blue = [4,int(rows/2.0)]
    current_loc_red = [columns-5,int(rows/2.0)]
    cells[current_loc_blue[0]][current_loc_blue[1]] = 1
    cells[current_loc_red[0]][current_loc_red[1]] = -2
    current_dir_blue = 'n'
    current_dir_red = 's'
    alive_red = True
    alive_blue = True
    paused = False


def rectangle(x1, y1, x2, y2):
    pyglet.graphics.draw(4, GL_QUADS, ('v2f', (x1 + brdr_sz, y1 + brdr_sz, x1 + brdr_sz, y2 + brdr_sz, x2 + brdr_sz, y2 + brdr_sz, x2 + brdr_sz, y1 + brdr_sz)))

def get_label(m,fs):
    return pyglet.text.Label(m, font_name='Comic Sans', font_size=fs, x=win.width//2, y=win.width//2, width=win.width, multiline=True) #anchor_x='center', anchor_y='center')


def run():
    # evolve the grid
    global cells, current_loc
    
    if current_dir_red == 'n':
        current_loc_red[0] += 1
    elif current_dir_red == 'e':
        current_loc_red[1] += 1
    elif current_dir_red == 's':
        current_loc_red[0] -= 1
    elif current_dir_red == 'w':
        current_loc_red[1] -= 1
        
    if current_dir_blue == 'n':
        current_loc_blue[0] += 1
    elif current_dir_blue == 'e':
        current_loc_blue[1] += 1
    elif current_dir_blue == 's':
        current_loc_blue[0] -= 1
    elif current_dir_blue == 'w':
        current_loc_blue[1] -= 1
        
    # periodic boundary conditions    
    current_loc_blue[0] = current_loc_blue[0] % rows  
    current_loc_red[0] = current_loc_red[0] % rows
    
    current_loc_blue[1] = current_loc_blue[1] % columns
    current_loc_red[1] = current_loc_red[1] % columns
    
    cells[current_loc_red[0]][current_loc_red[1]] -= 2 # -2 on a cell makes it red 
    cells[current_loc_blue[0]][current_loc_blue[1]] += 1 # 1 on a cell makes it blue       

    
def draw_grid(r, c, cell_sz):
    
    glColor4f(0.5, 0.5, 0.5, 1.0)

    #Horizontal lines
    for i in range(r+1):
        pyglet.graphics.draw(2, GL_LINES, ('v2i', (brdr_sz, i * cell_sz + brdr_sz, brdr_sz + window_width, i * cell_sz + brdr_sz)))
    #Vertical lines
    for j in range(c+1):
        pyglet.graphics.draw(2, GL_LINES, ('v2i', (j * cell_sz + brdr_sz, brdr_sz, j * cell_sz + brdr_sz, brdr_sz + window_height)))


def draw():
    global alive_red, alive_blue
    # Clear buffers
    glClear(GL_COLOR_BUFFER_BIT)
    
    if state == 'start':
        draw_controls()
    
    if state == 'game':

        draw_grid(rows, columns, cell_size)
        
        rownum = 0
        for f in range(len(cells)):
            
            current_row = cells[f]
            colnum = 0
            for c in range(len(current_row)):
                if current_row[c] == 1:
                    glColor4f(0.0, 0.0, 1.0, 1.0)
                    rectangle(c * cell_size, f * cell_size,
                              c * cell_size + cell_size, f * cell_size + cell_size)
                elif current_row[c] == -2:
                    glColor4f(1.0, 0.0, 0.0, 1.0)
                    rectangle(c * cell_size, f * cell_size,
                              c * cell_size + cell_size, f * cell_size + cell_size)
                elif current_row[c] == -4:
                    glColor4f(1.0, 1.0, 0.0, 1.0)
                    rectangle(c * cell_size, f * cell_size,
                              c * cell_size + cell_size, f * cell_size + cell_size)
                    alive_red = False
                elif current_row[c] == 2:
                    glColor4f(0.0, 1.0, 1.0, 1.0)
                    rectangle(c * cell_size, f * cell_size,
                              c * cell_size + cell_size, f * cell_size + cell_size)
                    alive_blue = False
                elif current_row[c] == -1:
                    glColor4f(1.0, 0.0, 1.0, 1.0)
                    rectangle(c * cell_size, f * cell_size,
                              c * cell_size + cell_size, f * cell_size + cell_size)
                    if rownum == current_loc_red[0] and colnum == current_loc_red[1]: 
                        alive_red = False

                    if rownum == current_loc_blue[0] and colnum == current_loc_blue[1]: 
                        alive_blue = False

                colnum+=1
            rownum+=1
            
'''            
# Create your AI function here
def urAI():
    # The only global that you can modify is current_dir_red
    global current_dir_red
    # You can use cells, and current_loc_red they just cannot be modified. 
    # You must assign current_dir_red a valid direction 'n', 's', 'e', or 'w'
    # Your function must not be too computationally demanding. Keep it under a second.
'''    
            
# The dumb self-avoiding walk          
def AI():
    global current_dir_red
    choices = []
    if cells[(current_loc_red[0]+1)%rows][(current_loc_red[1])%columns]==0:
        choices.append('n')
    if cells[(current_loc_red[0]-1)%rows][(current_loc_red[1])%columns]==0:
        choices.append('s')
    if cells[(current_loc_red[0])%rows][(current_loc_red[1]+1)%columns]==0:
        choices.append('e')
    if cells[(current_loc_red[0])%rows][(current_loc_red[1]-1)%columns]==0:
        choices.append('w')
    if choices:
        current_dir_red = random.choice(choices)
        
# The run and hide: go in the direction with the most open space. 
def AI2():
    global current_dir_red
    choices = [0,0,0,0]
    dir = ('n','s','e','w')
    
    for i in range(1,rows):
        if cells[(current_loc_red[0]+i)%rows][(current_loc_red[1])%columns]==0:
            choices[0]+=1
        else:
            break
    for i in range(1,rows):
        if cells[(current_loc_red[0]-i)%rows][(current_loc_red[1])%columns]==0:
            choices[1]+=1
        else:
            break
    for i in range(1,columns):
        if cells[(current_loc_red[0])%rows][(current_loc_red[1]+i)%columns]==0:
            choices[2]+=1
        else:
            break
    for i in range(1,columns):
        if cells[(current_loc_red[0])%rows][(current_loc_red[1]-i)%columns]==0:
            choices[2]+=1
        else:
            break

    current_dir_red = dir[choices.index(max(choices))]

@win.event
def on_key_press(symbol, modifiers):
    global state, paused, current_dir_blue, current_dir_red
    if symbol == key.X:
        if state == 'start':
            state = 'game'
            
        elif state == 'game':
            state = 'start'
            startup()
            
    if state == 'game':
        # check for keyboard input. do not allow backtracking aka suicide. 
        if symbol == key.W:
            if current_dir_blue != 's':
                current_dir_blue = 'n'
        if symbol == key.A:
            if current_dir_blue != 'e':
                current_dir_blue = 'w'
        if symbol == key.S:
            if current_dir_blue != 'n':
                current_dir_blue = 's'
        if symbol == key.D:
            if current_dir_blue != 'w':
                current_dir_blue = 'e'    
        if symbol == key.UP:
            if current_dir_red != 's':
                current_dir_red = 'n'
        if symbol == key.LEFT:
            if current_dir_red != 'e':
                current_dir_red = 'w'
        if symbol == key.DOWN:
            if current_dir_red != 'n':
                current_dir_red = 's'
        if symbol == key.RIGHT:
            if current_dir_red != 'w':
                current_dir_red = 'e' 
            
            
        if symbol == key.SPACE:
            # pause/unpause game
            paused = not paused

# set speed 
pyglet.clock.set_fps_limit(10)


startup()
run_tick = True
while not win.has_exit:

    win.dispatch_events()
    dt = pyglet.clock.tick()
    
    # set title framerate
    win.set_caption('Lighter Cycles v0.1')
    
    if state == 'start':
        draw()

    if state == 'game' and not paused and alive_red and alive_blue: 
        # Insert Your AI function call here 
        AI2()
        # allow two clock tics between movements to allow for user to change direction more easily 
        if run_tick:
            run()
        draw()
    if not alive_red or not alive_blue:
        if alive_red:
            label = get_label('RED WINS!\n\n\n PRESS X TO RESET',64)
        elif alive_blue:
            label = get_label('BLUE WINS!\n\n\n PRESS X TO RESET',64)
        else:
            label = get_label('IT\'S A TIE!\n\n\n PRESS X TO RESET',64)
        label.draw()
         
    win.flip()
    run_tick = not run_tick
    